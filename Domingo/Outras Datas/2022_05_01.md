# Hinos de domingo - 01/05/2022



## Irmãs

- [Camilly Vitória - Aquieta-te](https://www.youtube.com/watch?v=Wn-Qz-pYxDE) | [Altura](https://gitlab.com/Rudrigo96Y/igreja-parque-brasil-musicos/-/tree/main/Hinos%20J%C3%A1%20cantados/Irm%C3%A3s)
- [Rayssa e Ravel - Ele Me Viu](https://www.youtube.com/watch?v=Hjcf5Zqn-IY) | [Altura](https://gitlab.com/Rudrigo96Y/igreja-parque-brasil-musicos/-/tree/main/Hinos%20J%C3%A1%20cantados/Irm%C3%A3s)

## Jovens

- [Soraya Moraes - Caminho no deserto - Playback 1 tom abaixo](https://www.youtube.com/watch?v=AfQEj7az1f4) | [Altura](https://gitlab.com/Rudrigo96Y/igreja-parque-brasil-musicos/-/tree/main/Hinos%20J%C3%A1%20cantados/Jovens)

## Adolescentes

- [Isadora Pompeo - Seja Forte](https://www.youtube.com/watch?v=-LQEZynyqYM&feature=youtu.be) | [Altura](https://gitlab.com/Rudrigo96Y/igreja-parque-brasil-musicos/-/tree/main/Hinos%20J%C3%A1%20cantados/Adolescentes)
