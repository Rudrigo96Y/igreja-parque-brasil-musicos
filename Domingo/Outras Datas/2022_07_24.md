# Hinos de domingo - 24/07/2022



## Irmãs

- [Damares - O Convidado](https://www.youtube.com/watch?v=6T_Yret4NRw) | [Altura](https://gitlab.com/Rudrigo96Y/igreja-parque-brasil-musicos/-/tree/main/Hinos%20J%C3%A1%20cantados/Irm%C3%A3s)

## Jovens

- [Canção e Louvor - Maravilhoso](https://www.youtube.com/watch?v=97vSkZagC0E)
- [Canção e Louvor - Rei e Santo](https://www.youtube.com/watch?v=TydJgKbt90o)
- [Canção e Louvor - Salmos 24](https://www.youtube.com/watch?v=s9H4_MNsqPg)
- [Renascer Praise XII Apostólico - Na Força Do Louvor](https://www.youtube.com/watch?v=QDVP_AvoIhc)

## Adolescentes
