# Hinos de domingo - 19/06/2022



## Irmãs

- [Shirley Carvalhares - Vendavais](https://www.youtube.com/watch?v=ByY8Vfa2zNY&feature=youtu.be) | [Altura](https://gitlab.com/Rudrigo96Y/igreja-parque-brasil-musicos/-/tree/main/Hinos%20J%C3%A1%20cantados/Irm%C3%A3s)

## Jovens

- [Gabriela Rocha - Eu Navegarei - 1 tom](https://www.youtube.com/watch?v=nomHRHXfb6k) | [Altura](https://gitlab.com/Rudrigo96Y/igreja-parque-brasil-musicos/-/tree/main/Hinos%20J%C3%A1%20cantados/Jovens)

## Adolescentes

- [Paulo Neto - Tua Presença](https://www.youtube.com/watch?v=gs-V3_9eqX8) | [Altura](https://gitlab.com/Rudrigo96Y/igreja-parque-brasil-musicos/-/tree/main/Hinos%20J%C3%A1%20cantados/Adolescentes)
