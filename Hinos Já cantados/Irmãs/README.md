# Hinos que as irmãs já cantaram



## Aline Barros

- [Bem Mais do que Tudo](https://www.youtube.com/watch?v=vQhzie9WEuU) | [Original: G](https://www.cifraclub.com.br/aline-barros/bem-mais-que-tudo/)



## Cassiane

- [Com Muito Louvor](https://www.youtube.com/watch?v=IjtBtWLn6B8) | Altura: Bb / [Original: Bb](https://www.cifraclub.com.br/cassiane/com-muito-louvor/)



## Damares

- [O Convidado](https://www.youtube.com/watch?v=6T_Yret4NRw) | Altura: C#m / [Original C#m](https://www.cifraclub.com.br/damares/o-convidado/)



## Fabiana Anastácio

- [Até o Fim](https://www.youtube.com/watch?v=vQhzie9WEuU) | Altura: A# / Original: A#
- [Quem Me Vê Cantando](https://www.youtube.com/watch?v=yjxfRDiz08Y) | [Original B](https://www.cifraclub.com.br/fabiana-anastacio/quem-me-ve-cantando/)



## Lauriete

- [Igual Não Há](https://www.youtube.com/watch?v=upJLQFZaBZQ) | Altura: Bb / [Original: F#](https://www.cifraclub.com.br/lauriete/igual-nao-ha/)
- [João Viu](https://www.youtube.com/watch?v=kXz8_0iX75Q) | Altura: F / Original: G



## Sarah Farias

- [Renovo](https://www.youtube.com/watch?v=jApvWOGhKyo) | Altura: A | [Original: A](https://www.cifraclub.com.br/sarah-farias/renovo/)



## Shirley Carvalhares

- [Vendavais](https://www.youtube.com/watch?v=ByY8Vfa2zNY&feature=youtu.be) | Altura: Cm | [Original: Dm](https://www.cifraclub.com.br/shirley-carvalhaes/vendavais/)



## Rayanne Vanessa

- [O Mestre no Comando](https://www.youtube.com/watch?v=UN_sABzxfh0) | Altura: D / [Original: D](https://www.cifraclub.com.br/rayanne-vanessa/o-mestre-no-comando/)



## Rayssa e Ravel

- [Ele Me Viu](https://www.youtube.com/watch?v=Hjcf5Zqn-IY) | Altura: B / Original: B
