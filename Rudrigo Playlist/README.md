# Rudrigo Playlist 



## Bruna Karla

- [Lugar Santo](https://www.youtube.com/watch?v=oAsqsv9tcMo)



## Comunidade Evangélica Internacional da Zona Sul

- [Rompendo em Fé](https://www.youtube.com/watch?v=6qKo9ZkhtWw)
- [Rompendo em Fé + Aline Barros](https://www.youtube.com/watch?v=3iwWQJk6OBw)



## Coral Kemuel

- [Ele Vem](https://www.youtube.com/watch?v=X_4PDyoH_X8)



## Daniela Araújo + Jorge Araújo e Eula Paula

- (Cicatrizes Part.)[https://www.youtube.com/watch?v=P6xtAq_QLwE&list=PLF4Oop5uX38cvh_PP2JaBpQb9Rdh5Bvr3&index=42]



## Danielle Cristina

- [Vença a Prova](https://www.youtube.com/watch?v=e4MNQi7nSzI)



## Denis Cruz Drummer

- [Dias de Elias](https://www.youtube.com/watch?v=v8A2iab5ar0)



## Diante do Trono 3

- [Águas Purificadoras](https://www.youtube.com/watch?v=ziR49ui-G28)



## Jamily

- [Deus é Maior](https://www.youtube.com/watch?v=yO8A9HF-BoY)



## Ludmila Ferber

- [Buscar Tua Face é Preciso e Espontâneo](https://www.youtube.com/watch?v=wABKGxS_v40)
- [Recebe a Cura](https://www.youtube.com/watch?v=1VjIAuWhv4E)



## Mara Lima

- [Daniel](https://www.youtube.com/watch?v=8X_8IkEXpvE&list=PLF4Oop5uX38cvh_PP2JaBpQb9Rdh5Bvr3&index=52)



## Nani Azevedo
- [Adorador por Excelência](https://www.youtube.com/watch?v=JV2LHHD3cGw)



## Voz da Verdade

- [A Hora do Salvador](https://www.youtube.com/watch?v=C1INa7jqshw&list=PLF4Oop5uX38cvh_PP2JaBpQb9Rdh5Bvr3&index=43)
- [Além do Rio Azul](https://www.youtube.com/watch?v=QpBNXkKIYZw&list=PLF4Oop5uX38cvh_PP2JaBpQb9Rdh5Bvr3&index=24)



## Harpa Cristã

- [004 - Deus Velará Por Ti](https://www.youtube.com/watch?v=MO9e-e_0he0)
- [005 - Ó Desce, Fogo Santo](https://www.youtube.com/watch?v=C2Cp2DiFij4)
- [131 - De Valor em Valor](***)
- [186 - De Valor Em Valor](https://www.youtube.com/watch?v=Gz4_SwGPlfA)
- [350 - A História da Cruz](https://www.youtube.com/watch?v=f9xxGH_ksEU)
- [370 - Grato a Ti](https://www.youtube.com/watch?v=PZUjVVXIYCQ)
- [475 - Ele Sofre Por Mim](https://www.youtube.com/watch?v=5IhlSJxFuZs)
- [484 - Meus Pecados Levou](https://www.youtube.com/watch?v=fHMVgaGABFg)
